@extends('layout/main')

@section('title', 'Tambah data Mahasiswa')

@section('container')
<div class="container">
    <div class="row">
       <div class="col-6">
           <h1 class="mt-3" >Tambah Mahasiswa</h1>

           <form method="post" action="/students">
           @csrf
             <div class="form-group">
                <label for="nrp">Nrp</label>
                <input type="text" class="form-control @error('nrp') is-invalid @enderror" id="nrp" placeholder="NRP" name="nrp" value="{{ old('nrp') }}">
                @error('nrp')
                <div class="invalid-feedback">{{ $message }}</div>
                @enderror
             </div>
             <div class="form-group">
                <label for="nama">Nama</label>
                <input type="text" class="form-control" id="nama" placeholder="Nama Lengkap" name="nama" value="{{ old('nama') }}">
             </div>
             <div class="form-group">
                <label for="email">Email</label>
                <input type="text" class="form-control" id="email" placeholder="email" name="email" value="{{ old('email') }}">
             </div>
             <div class="form-group">
                <label for="jurusan">Jurusan</label>
                <input type="text" class="form-control" id="jurusan" placeholder="Jurusan" name="jurusan" value="{{ old('email') }}">
             </div>
             <button type="seubmit" class="btn btn-primary">Tambah Data</button>
            </form>

    
       </div>
    </div>
 </div>
 @endsection