@extends('layout/main')

@section('title', 'Detail Mahasiswa')

@section('container')
<div class="container">
    <div class="row">
       <div class="col-7">
           <h1 class="mt-3" >Daftar Mahasiswa</h1>

        <div class="card">
        <div class="card-body">
        <h5 class="card-title">{{ $student->nrp }}</h5>
        <h6 class="card-subtitle mb-2 text-muted">{{ $student->nama }}</h6>
        <h6 class="card-subtitle mb-2 text-muted">{{ $student->email }}</h6>
        <h6 class="card-subtitle mb-2 text-muted">{{ $student->jurusan }}</h6>
        
        <button type="submit" class="btn btn-primary">Edit</button>
        <form action="{{ $student->id }}" method ="post" class="d-inline"> 
        @method('delete')
        @csrf
        <button type="submit" class="btn btn-danger">Delete</button></form>

        <a href="/students" class="card-link">Back</a>
  </div>
</div>
    
       </div>
    </div>
 </div>
 @endsection