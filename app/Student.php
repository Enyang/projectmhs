<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $fillable = ['nrp', 'nama', 'email', 'jurusan']; // yang boleh diisi
    //protected $guarded = ['id']; // dilindungi
    
}
